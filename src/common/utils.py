# System
from string import ascii_lowercase, digits
from random import choice
from hashlib import md5
# Django
from django.utils.text import slugify
from django.utils import timezone


def random_string_generator(size = 6, chars = ascii_lowercase + digits):
    """ 
    Returns a random string composed by lowercase letters and digits

    @size(INT): Maximum length of the string to be generated.
    @chars(STR): Scope of strins available to generate random strings.
    """ 
    return ''.join(choice(chars) for _ in range(size)) 
  
def unique_slug_generator(instance, new_slug = None):
    """ 
    If given '@new_slug' it will be returned, else, if @instance has
    attr 'title' a queryset will find for its existance as slug,
    if that object exists, a new random string preceded by a '-' will
    be added to the original slug in use. Then that new slug will be 
    submitted to a new queryset, until it is unique.

    Also if @instance has no title attr, a hashed timezone.now() will
    be used instead as initial slug. 

    @instance(): #TODO
    @new_slug(STR): If desired slug wants be used.
    """ 
    if new_slug is not None: 
        slug = new_slug 
    else: 
        if hasattr(instance, 'title'):
            slug = slugify(instance.title)
        else:
            to_hash = str(timezone.now().__str__()).encode('utf-8')

            slug = md5(to_hash).hexdigest()[:6]
    
    Klass = instance.__class__ 
    qs_exists = Klass.objects.filter(slug = slug).exists() 

    if qs_exists: 
        new_slug = "{slug}-{randstr}".format( 
            slug = slug,
            randstr = random_string_generator()
        ) 
           
        return unique_slug_generator(instance, new_slug = new_slug) 
    return slug 
