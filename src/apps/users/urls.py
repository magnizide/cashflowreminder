# Django
from django.urls import path

# View
from apps.users import views

urlpatterns = [
    # Management
    path(
        route='login/',
        view=views.LoginView.as_view(),
        name='login'
    ),
    path(
        route='logout/',
        view=views.LogoutView.as_view(),
        name='logout'
    ),
    path(
        route='signup/',
        view=views.SignupView.as_view(),
        name='signup'
    ),
    path(
        route='update/',
        view=views.UpdateUserView.as_view(),
        name='update'
    ),
]
