from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required(login_url='/user/login')
def homePage(request):
    template_name = 'users/homepage.html'

    return render(request, template_name)