"""Users views."""

# Django
from django.views.generic import DetailView, FormView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import views as auth_views
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
# Local
from .forms import SignupForm
from .models import CustomUser
from apps.bills.models import Bill


class SignupView(FormView):
    """Users sign up view."""

    template_name = 'users/signup.html'
    form_class = SignupForm
    success_url = reverse_lazy('user:login')

    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('home'))
        return self.render_to_response(self.get_context_data())

    def form_valid(self, form):
        """Save form data."""
        form.save()
        return super().form_valid(form)


class LoginView(auth_views.LoginView):
    """Login view."""

    redirect_authenticated_user = True
    template_name = 'users/login.html'


class LogoutView(LoginRequiredMixin, auth_views.LogoutView):
    """Logout view."""

    # template_name = 'users/logged_out.html'

class UpdateUserView(LoginRequiredMixin, UpdateView):
    """Update profile view."""

    template_name = 'users/update_user.html'
    model = CustomUser
    fields = ['first_name', 'last_name']

    def get_object(self):
        """Return user."""
        print(self.request.user.pk)
        return self.request.user

    def get_success_url(self):
        """Return to user's profile."""
        return reverse('home')


class UserDetailView(LoginRequiredMixin, DetailView):
    """User detail view."""

    template_name = 'users/detail.html'
    slug_field = 'pk'
    slug_url_kwarg = 'pk'
    queryset = CustomUser.objects.all()
    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        """Add user's posts to context."""
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        context['bills'] = Bill.objects.filter(user=user).order_by('-created')
        return context

