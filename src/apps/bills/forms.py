# Django 
from django.utils.translation import gettext as _
from django.utils import timezone
from django import forms
# Local
from .models import Bill, Fixed


def obj_lt_obj_add_error(instance, dicto, key1, key2, msg='%(greater)s is greater than %(less)s'):
    if (key1 in dicto) and (key2 in dicto):
        if (dicto[key1] or dicto[key1] == 0)  and dicto[key2]:
            if dicto[key1] < dicto[key2]:
                instance.add_error(key2, forms.ValidationError(_(msg), code='invalid', params={'greater':dicto[key2], 'less': dicto[key1]}))


class DateInput(forms.DateInput):
    input_type = 'date'


class BillCreateForm(forms.ModelForm):
    """ Docstring """
    class Meta:
        model = Bill
        exclude = ['author', 'slug']

    def clean(self):
        cleaned_data = super().clean()
        
        if cleaned_data['is_automated'] is False and cleaned_data['min_payment_amount'] > 0:
            self.add_error(None, forms.ValidationError(_("Minimum payment amount must be set to 0 if this bill is not Automated."), code='invalid'))
        elif cleaned_data['is_automated'] is True and cleaned_data['min_payment_amount'] == 0:
            self.add_error(None, forms.ValidationError(_("Minimum payment amount must be different than 0 if this bill is Automated."), code='invalid'))
        

class FixedCreateForm(forms.ModelForm):
    """ Docstring """
    class Meta:
        model = Fixed
        exclude = ['author', 'slug']
        widgets = {
            'debt_start_date' : DateInput(),
            'debt_end_date' : DateInput()
        }
    
    def clean(self):
        cleaned_data = super().clean()
        fields_to_check = (
            ("total_amount", "min_payment_amount"),
        )
        for field_pair in fields_to_check:
            obj_lt_obj_add_error(self, cleaned_data, *field_pair)
        
        if not(cleaned_data['debt_start_date'].day == cleaned_data['max_due_day']):
            self.add_error('max_due_day', forms.ValidationError(_("%(great)s, must be equal as the day in %(less)s"), code='invalid', params={'less':cleaned_data['debt_start_date'], 'great': cleaned_data['max_due_day']}))
        
        return cleaned_data