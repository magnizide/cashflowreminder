# System 
# Dajngo
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Avg, Sum, Min, Max
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView
from django.core.paginator import Paginator
# Local
from apps.bills.models import Bill, Fixed
from apps.payments.models import Payment
from apps.bills.forms import BillCreateForm, FixedCreateForm


class BillListView(LoginRequiredMixin, TemplateView):
    """ Docstring """
    template_name = 'bills/bill_list.html'

    def get_context_data(self, **kwargs):
        context = super(BillListView, self).get_context_data(**kwargs)
        bills = Bill.objects.filter(author=self.request.user)    
        context['bills'] = bills
        return context


class BillDetailView(LoginRequiredMixin, DetailView):
    """ Docstring """
    template_name = 'bills/bill_detail.html'
    context_object_name = 'bill'

    def get_object(self):
        bill = Bill.objects.get(slug=self.kwargs['slug'])
        if bill.author == self.request.user:
            if hasattr(bill, 'fixed'):
                return bill.fixed
            else:
                return bill
        else:
            raise PermissionDenied('Object {} cannot be viewed by {}'.format(bill, self.request.user))
         
    def get_context_data(self, **kwargs):
        # Retrieves the 'super' get_context
        context = super(BillDetailView, self).get_context_data(**kwargs)
        # Queries, Filters and Order all payments attached to the Bill Identified by self.kwargs['slug']
        payments_queryset = Payment.objects.filter(bill__slug=self.kwargs['slug']).order_by('max_payment_date')
        # Creates the Paginator for the previous QS
        paginator = Paginator(payments_queryset, 12)
        # Get the current page of the page
        page = self.request.GET.get('page')
        # Assign that page to the template
        payments = paginator.get_page(page)
        # Load the paginated QS in the context
        context['payments'] = payments
        # Perform aggregate actions on the following QS
        payments_gt_zero = payments_queryset.filter(amount__gt=0)
        payments_extra_info = payments_gt_zero.aggregate(
            Sum('amount'),
            Avg('amount'),
            Min('amount'),
            Max('amount')
        )
        # If previus QS exists return its values, else take every aggregation and turn their values to 0
        payments_extra_info = payments_extra_info if payments_gt_zero.exists() else payments_extra_info.fromkeys(payments_extra_info.keys(), 0)
        # Update the context with the previously parsed data
        context.update(payments_extra_info)
        return context


class BillUpdateView(LoginRequiredMixin, UpdateView):
    """ Docstring """
    success_url = reverse_lazy('bill:list')
    template_name = 'bills/bill_form.html'
    context_object_name = 'bill'

    def get_object(self):
        bill = Bill.objects.get(slug=self.kwargs['slug'])
        if bill.author == self.request.user:
            if hasattr(bill, 'fixed'):
                return bill.fixed
            else:
                return bill
        else:
            raise PermissionDenied('Object {} cannot be edited by {}'.format(bill, self.request.user))
    
    def get_form_class(self):
        obj = self.get_object()
        if hasattr(obj, 'fixed'):
            return FixedCreateForm
        else:
            return BillCreateForm


class BillDeleteView(LoginRequiredMixin, DeleteView):
    """ Docstring """
    success_url = reverse_lazy('bill:list')
    template_name = 'bills/bill_confirm_delete.html'
    context_object_name = 'bill'

    def get_object(self):
        bill = Bill.objects.get(slug=self.kwargs['slug'])
        if hasattr(bill, 'fixed'):
            return bill.fixed
        else:
            return bill

# 'Regular' bill type views - - - - - 
class FixedCreateView(LoginRequiredMixin, CreateView):
    """ Docstring """
    model = Fixed
    success_url = reverse_lazy('bill:list')
    form_class = FixedCreateForm
    template_name = 'bills/bill_create_form.html'

    def form_valid(self, form):
        """ Docstring
            @form2complete = stores the current values typed in the form.
            @form2complete.user = auto populates the user param from the model
                with the current logged user lazy_object.
        """
        form2complete = form.save(commit=False)
        form2complete.author = self.request.user
        form2complete.save()
        return super().form_valid(form)
        

# 'Service' bill type views - - - - - 
class BillCreateView(LoginRequiredMixin, CreateView):
    """ Docstring """
    model = Bill
    success_url = reverse_lazy('bill:list')
    form_class = BillCreateForm
    template_name = 'bills/bill_create_form.html'

    def form_valid(self, form):
        """ Docstring
            @form2complete = stores the current values typed in the form.
            @form2complete.user = auto populates the user param from the model
                with the current logged user lazy_object.
        """
        form2complete = form.save(commit=False)
        form2complete.author = self.request.user
        form2complete.save()
        return super().form_valid(form)
