# Django
from django.urls import path
# Local
from apps.bills import views

urlpatterns = [
    path(
        route = '',
        view = views.BillListView.as_view(),
        name = 'list'
    ),
    path(
        route = 'detail/<slug:slug>',
        view = views.BillDetailView.as_view(),
        name = 'detail'
    ),
    path(
        route = 'edit/<slug:slug>',
        view = views.BillUpdateView.as_view(),
        name = 'edit'
    ),
    path(
        route = 'delete/<slug:slug>',
        view = views.BillDeleteView.as_view(),
        name = 'delete'
    ),
    # Regular Bills
    path(
        route = 'create-bill/',
        view = views.FixedCreateView.as_view(),
        name = 'create-bill'
    ),
    # Service Bill
    path(
        route = 'create-service-bill/',
        view = views.BillCreateView.as_view(),
        name = 'create-service-bill'
    ),
]