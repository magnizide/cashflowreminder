# Django
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
# Local
from apps.bills.models import Bill, Fixed
from apps.bills.utils import claculate_instalments_info
from apps.payments.models import Payment
from common import utils

# - - - - - Pre Save Signals - - - - -
@receiver(pre_save, sender=Bill)
def bill_pre_save_receiver(sender, instance, *args, **kwargs):
    """Docstring""" 
    if not instance.slug: 
        instance.slug = utils.unique_slug_generator(instance)
    
    if not instance.grace_days:
        instance.grace_days = 3

@receiver(pre_save, sender=Fixed)
def fixed_pre_save_receiver(sender, instance, *args, **kwargs):
    """Docstring""" 
    if not instance.slug: 
        instance.slug = utils.unique_slug_generator(instance)
    if not instance.grace_days:
        instance.grace_days = 3

# - - - - - Post Save Signals - - - - -
@receiver(post_save, sender=Fixed)
def fixed_post_save_receiver(sender, instance, *args, **kwargs):
    """ Docstring """
    if len(Payment.objects.filter(bill=instance)) == 0:
        instalments_info = claculate_instalments_info(instance.debt_start_date, instance.instalments)
        for info in instalments_info:
            payment = Payment(bill=instance, max_payment_date=info[0], amount=0, expired=info[1])
            payment.save()