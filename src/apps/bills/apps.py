from django.apps import AppConfig


class BillsConfig(AppConfig):
    name = 'apps.bills'

    def ready(self):
        import apps.bills.signals
