# System
from datetime import date
from calendar import monthrange
# Django
from django.utils import timezone

def claculate_instalments_info(start_date, instalments):
    """ Docstring """
    year = start_date.year
    month = start_date.month
    day = start_date.day
    current_date = timezone.now().date()
    for i in range(instalments):
        if month > 12:
            year += 1
            month = 1
        
        try:
            instalment = date(year,month,day)
        except ValueError:
            days_in_month = monthrange(year, month)[1]
            instalment = date(year,month,days_in_month)

        expired = False
        if current_date > instalment:
            expired = True

        month+=1
        yield instalment, expired