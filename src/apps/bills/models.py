# System Imports

# Django Imports
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone
from django.db import models
# Local Imports
from apps.users.models import CustomUser


class Bill(models.Model):
    """ Docstring """
    # Foreign Key ------
    author = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        editable=False
    )
    # Foreign Key END ------
    slug = models.SlugField(
        max_length = 6,
        null = True,
        editable=False,
    ) 
    title = models.CharField(
        max_length=50,
    )
    is_automated = models.BooleanField(
        default=False,
    )
    is_active = models.BooleanField(
        default=True,
    )
    min_payment_amount = models.DecimalField(
        default=0,
        max_digits=11,
        decimal_places=2,
        validators = [MinValueValidator(0)]
    )
    max_due_day = models.PositiveIntegerField(
        validators=[
            MinValueValidator(1),
            MaxValueValidator(31)
        ],
        help_text='Service bill: add the max day for this bill to be paid.\nRegular bill: must use the day of start date.'
    )
    grace_days = models.PositiveIntegerField(
        blank=True,
        null=True,
        default=3,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(5)
        ]
    )
    description = models.TextField(
        blank=True
    )
    # Meta Data
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class Fixed(Bill):
    """ Docstring """
    debt_start_date = models.DateField()
    instalments = models.PositiveSmallIntegerField(
        validators = [
            MinValueValidator(0),
            MaxValueValidator(400)
        ],
    )
    total_amount = models.DecimalField(
        max_digits=11,
        decimal_places=2,
        validators=[MinValueValidator(0)]
    )    