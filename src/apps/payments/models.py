# System
# Django
from django.core.validators import MinValueValidator
from django.utils import timezone
from django.db import models
# Local
from apps.bills.models import Bill

# Create your models here.
class Payment(models.Model):
    """Docstring"""
    # Foreign Key ------
    bill = models.ForeignKey(
        Bill,
        on_delete=models.CASCADE,
        related_name='Payment_bill',
        editable=False,
    )
    # Foreign Key END ------
    slug = models.SlugField(
        max_length = 13,
        null = True,
        blank = True,
        editable=False
    )
    max_payment_date = models.DateField()
    outgoing_payment_date = models.DateField(
        blank=True,
        null=True,
    )
    amount = models.DecimalField(
        blank=False,
        max_digits=11,
        decimal_places=2,
        validators=[MinValueValidator(0)]
    )
    expired = models.BooleanField(
        default=False,
    )

    # Meta Data
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
