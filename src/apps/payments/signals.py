# Django
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
# Local
from apps.payments.models import Payment
from common import utils

#----- Pre Save Signals -----
@receiver(pre_save, sender=Payment)
def payment_pre_save_receiver(sender, instance, *args, **kwargs):
        """Docstring""" 
        if not instance.slug: 
            instance.slug = utils.unique_slug_generator(instance)