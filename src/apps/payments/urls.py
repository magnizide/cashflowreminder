# Django
from django.urls import path
# Local
from apps.payments import views

urlpatterns = [
    # Payments URLs
    path(
        route = 'add-to-bill/<slug:bill_slug>',
        view = views.PaymentCreateView.as_view(),
        name = 'add'
    ),
    path(
        route = 'edit/<slug:slug>',
        view = views.PaymentUpdateView.as_view(),
        name = 'edit'
    ),
    path(
        route = 'delete/<slug:slug>',
        view = views.PaymentDeleteView.as_view(),
        name = 'del'
    )
]