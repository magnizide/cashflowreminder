# System
# Django
from django.utils.translation import ugettext as _
from django.utils import timezone
from django import forms
# Local
from apps.payments.models import Payment


current_date = timezone.now().date()

class DateInput(forms.DateInput):
    input_type = 'date'


class PaymentCreateForm(forms.ModelForm):
    """ Docstring """

    class Meta:
        model = Payment
        exclude = ['bill']
        widgets = {
            'max_payment_date' : DateInput(),
            'outgoing_payment_date': DateInput(),
        }

    def __init__(self, *args, **kwargs):
        self.bill_instance = kwargs.pop('bill_instance')
        super(PaymentCreateForm, self).__init__(*args, **kwargs)

    def clean_amount(self):
        cleaned_amount = self.cleaned_data['amount']
        bill = self.bill_instance
        if hasattr(bill, 'fixed'):
            if cleaned_amount == 0:
                pass
            elif not(bill.fixed.min_payment_amount <= cleaned_amount <= bill.fixed.total_amount):
                self.add_error('amount', forms.ValidationError(_('This value must be in the range of your min payment and the total of your debt or equals to zero(Reset).'), code='invalid'))

        return cleaned_amount

    def clean(self):
        cleaned_data = super().clean()
        if (current_date > cleaned_data['max_payment_date']) and cleaned_data['expired'] is False:
            self.add_error('expired', forms.ValidationError(_('This value should be checked'), code='invalid'))
        elif (current_date <= cleaned_data['max_payment_date']) and cleaned_data['expired'] is True:
            self.add_error('expired', forms.ValidationError(_('This value should not be checked'), code='invalid'))
        
