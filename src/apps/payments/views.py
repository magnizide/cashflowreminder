# System
from calendar import monthrange
from datetime import date
# Django
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.shortcuts import render
from django.utils import timezone
# Local
from apps.payments.forms import PaymentCreateForm
from apps.payments.models import Payment
from apps.bills.models import Bill


class PaymentCreateView(LoginRequiredMixin, CreateView):
    """ Docstring """
    form_class = PaymentCreateForm
    template_name = 'payments/payment_create_form.html'

    def form_valid(self, form):
        """ Docstring
            @form2complete = stores the current values typed in the form.
            @form2complete.user = auto populates the user param from the model
                with the current logged user lazy_object.
        """
        form2complete = form.save(commit=False)
        bill = Bill.objects.filter(slug=self.kwargs['bill_slug'])
        if bill.exists():
            if bill.first().author == self.request.user:
                form2complete.bill = bill.first()
                form2complete.save()
            else:
                raise PermissionDenied('Object {} cannot be viewed by {}'.format(bill.first(), self.request.user))
        else:
            raise ObjectDoesNotExist('No object %s' % (self.kwargs['bill_slug']))

        return super().form_valid(form)
    
    def get_success_url(self):
        return reverse('bill:detail', kwargs={'slug':self.kwargs['bill_slug']})
    
    def get_context_data(self, **kwargs):
        context = super(PaymentCreateView, self).get_context_data(**kwargs)
        context['bill_slug'] = self.kwargs['bill_slug']
        return context
    
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        bill = Bill.objects.filter(slug=self.kwargs['bill_slug'])
        if bill.exists():
            kwargs.update({'bill_instance': bill.first()})

        return kwargs
    
    def get_initial(self):
        """Return the initial data to use for forms on this view."""
        initial = super().get_initial()
        bill = Bill.objects.filter(slug=self.kwargs['bill_slug']).first()
        if not hasattr(bill, 'fixed'):
            current_date = timezone.localdate()
            try:
                pre_max_due_day = date(current_date.year,current_date.month,bill.max_due_day)
            except ValueError:
                pre_max_due_day = date(current_date.year,current_date.month,monthrange(current_date.year, current_date.month)[1])
        
            initial.update({
            'max_payment_date': pre_max_due_day,
            })
            if bill.is_automated:
                initial.update({
                    'outgoing_payment_date': pre_max_due_day,
                    'amount': bill.min_payment_amount,
                    'expired': True if current_date > pre_max_due_day else False,
                })

        
        return initial


class PaymentUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'payments/payment_form.html'
    form_class = PaymentCreateForm

    def get_object(self):
        payment = Payment.objects.get(slug=self.kwargs['slug'])
        if payment.bill.author == self.request.user:
            return payment
        else:
            raise PermissionDenied('Object {} cannot be updatetd by {}'.format(payment, self.request.user))

    def get_success_url(self):
        payment = self.get_object()
        return reverse('bill:detail', kwargs={'slug':payment.bill.slug})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        bill = self.get_object().bill
        kwargs.update({'bill_instance': bill})

        return kwargs


class PaymentDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'payments/payment_confirm_delete.html'
    
    def get_object(self):
        payment = Payment.objects.get(slug=self.kwargs['slug'])
        if payment.bill.author == self.request.user:
            return payment
        else:
            raise PermissionDenied('Object {} cannot be delete by {}'.format(payment, self.request.user))
    
    def get_success_url(self):
        payment = self.get_object()
        return reverse('bill:detail', kwargs={'slug':payment.bill.slug})